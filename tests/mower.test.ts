import 'jasmine';
import { Mower } from '../src/mower';
import { Position } from '../src/position';

describe('Mower', () =>  {
    let mower: Mower;

    beforeAll(() => {
        mower = new Mower();
        mower.initLawn(6, 7);
    });

    describe('constructor', () => {
        it('shall instanciate a mower object', () => {
            expect(mower).toBeInstanceOf(Mower);
        });
    });

    describe('', () => {
        it('shall set the mower lower left and upper right corner coordinates', () => {
            const lawn = [{x: 0, y: 0}, {x: 6, y: 7}]
            expect(mower.getLawn()).toEqual(lawn);      
        });

        it('shall set the mower position', () => {
            mower.setPosition(3, 7, 'W');
            const position: Position = {
                x: 3,
                y: 7,
                orientation: 'W'
            };
            expect(mower.getPosition()).toEqual(position);
        });

        it('shall indicate if the mower can make a forward move or not', () => {
            mower.setPosition(1, 1, 'W');
            expect(mower.canMove()).toBeTrue;
            
            mower.setPosition(0, 0, 'S');
            expect(mower.canMove()).toBeFalse;
        });

        it('shall apply a move on the mower', () => {
            mower.setPosition(1, 1, 'W');
            mower.move('L');
            expect(mower.getPosition()).toEqual({x: 1, y: 1, orientation: 'S'});

            mower.move('F');
            expect(mower.getPosition()).toEqual({x: 1, y: 0, orientation: 'S'});

            mower.move('F');
            expect(mower.getPosition()).toEqual({x: 1, y: 0, orientation: 'S'});
        });
    });
});
import { format } from 'url';
