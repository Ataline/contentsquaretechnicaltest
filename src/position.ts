export interface Position {
    x: number;
    y: number;
    orientation: 'E'|'W'|'N'|'S';
}

export interface Coordinates {
    x: number;
    y: number;
}