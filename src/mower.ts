import { Position, Coordinates } from './position';

/**
 * Mower Class
 */
export class Mower {
    private lawnLowerLeftCorner: Coordinates;
    private lawnUpperRightCorner: Coordinates;
    private position: Position;

    public constructor() {
    }

    /**
     * Sets the coordinates of the lawn upper right and lower left corners
     * @param x coordinate of the upper right corner on the X axis
     * @param y coordinate of the upper rigth corner on the Y axis
     */
    public initLawn(x: number, y: number): void {
        this.lawnLowerLeftCorner = {
            x: 0,
            y: 0,
        };
        this.lawnUpperRightCorner = { x, y };
    }

    /**
     * Return the lawn lower left and upper right corner coordinates
     */
    public getLawn(): Coordinates[] {
        return [this.lawnLowerLeftCorner, this.lawnUpperRightCorner];
    }

    /**
     * Checks if the forward move is possible.
     */
    public canMove(): boolean {
        switch(this.position.orientation) {
            case 'W': return this.position.x - 1 >= this.lawnLowerLeftCorner.x;
            case 'E': return this.position.x + 1 <= this.lawnUpperRightCorner.x;
            case 'N': return this.position.y + 1 <= this.lawnUpperRightCorner.y;
            case 'S': return this.position.y - 1 >= this.lawnLowerLeftCorner.y;
        }  
    }

    /**
     * Applies a 90 degrees rotation or a forward move in regard to the given direction
     * @param direction The direction of the move
     */
    public move(direction: 'L'|'R'|'F'): void {
        const orientationsMap = {
            'L': {
                'N': 'W',
                'W': 'S',
                'S': 'E',
                'E': 'N',
            },
            'R': {
                'N': 'E',
                'E': 'S',
                'S': 'W',
                'W': 'N'
            }
        };

        const orientationAndPositionMap = {
            'N': () => this.verticalMove(1),
            'S': () => this.verticalMove(-1),
            'E': () => this.horizontalMove(1),
            'W': () => this.horizontalMove(-1),
        };

        switch (direction) {
            case 'F':
                if (this.canMove()) {
                    const currentOrientation: string = this.position.orientation;
                    orientationAndPositionMap[currentOrientation]();
                }
                break;
            default:
                const currentOrientation: string = this.position.orientation;
                this.position.orientation = orientationsMap[direction][currentOrientation];
        }
    }

    /**
     * Returns the current position of the mower.
     */
    public getPosition(): Position {
        return this.position;
    }

    /**
     * Sets the position of the mower to the given coordinates and orientation.
     * @param x The coordinate of the mower on the X axis.
     * @param y The coordinate of the mower on the Y axis.
     * @param orientation The orientation of the mower.
     */
    public setPosition(x: number, y: number, orientation: 'E'|'W'|'N'|'S'): void {
        this.position = { x, y, orientation };
    }

    /**
     * Performs a horizontal move on the mower.
     * @param dx The offset of the move.
     */
    private horizontalMove(dx: number): void {
        this.position.x += dx;
    }

    /**
     * Performs a vertical move on the mower.
     * @param dy The offset of the move.
     */
    private verticalMove(dy: number): void {
        this.position.y += dy;
    }
}