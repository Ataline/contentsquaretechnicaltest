import { readFileSync } from 'fs';
import { Mower } from './mower';

const initLawn = (mower: Mower, limits: string): void  => {
    const lawnLimits: number[] = limits.split(' ').map((v) => +v);
    mower.initLawn(lawnLimits[0], lawnLimits[1]);
};

const runMower = (mower: Mower, pos: string[], instructions: string): void => {
    mower.setPosition(Number(pos[0]), Number(pos[1]), pos[2] as 'E'|'W'|'N'|'S');

    instructions.split('').forEach((i: 'L'|'R'|'F') => {
            mower.move(i);
    });
    console.log(Object.values(mower.getPosition()).join(' '));
};

const run =  () => {
    const mowerA: Mower = new Mower();
    const mowerB: Mower = new Mower();
    
    const mowerAInstructions: string[] = [];
    const mowerBInstructions: string[] = [];
    
    let i: number = 0;
    let lawnLimits: string;

    // Read file
    readFileSync('./input.txt', 'utf8')
    .split('\n')
    .filter((line: string, index: number)  => {
        if (index === 0) lawnLimits = line;
        else if (index > 0  && index <= 2)  mowerAInstructions.push(line);
        else    mowerBInstructions.push(line);
    });

    initLawn(mowerA, lawnLimits);
    runMower(mowerA, mowerAInstructions[0].split(' '), mowerAInstructions[1]);
    initLawn(mowerB, lawnLimits);
    runMower(mowerB, mowerBInstructions[0].split(' '), mowerBInstructions[1]); 
};

run();